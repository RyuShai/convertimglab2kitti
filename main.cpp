#include <QCoreApplication>
#include <iostream>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QDomNodeList>
#include <QDir>
#include <QDirIterator>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
using namespace std;
QString trainPath ;
QString trainImage ;
QString trainLabel ;

QString valPath ;
QString valImage ;
QString valLabel;
void createTrainFolder(QString rootFolder)
{
     trainPath = rootFolder+"train/";
     trainImage = trainPath+"images/";
     trainLabel = trainPath+"labels/";

     valPath = rootFolder+"val/";
     valImage = valPath+"images/";
     valLabel = valPath+"labels/";
    QDir().mkpath(trainPath);
    QDir().mkpath(trainImage);
    QDir().mkpath(trainLabel);
    QDir().mkpath(valPath);
    QDir().mkpath(valImage);
    QDir().mkpath(valLabel);

}

void test(QString root, int waitkey)
{
    qDebug()<<root;
    QStringList listImage,listLabel;
    QDir dir(root);
    QDirIterator it(root, QStringList() << "*.jpg", QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext())
    {
        listImage.push_back(it.next());
    }
    QDirIterator it2(root, QStringList() << "*.txt", QDir::Files, QDirIterator::Subdirectories);
    while (it2.hasNext())
    {
        listLabel.push_back(it2.next());
    }
    int staffCount=0;
    bool pause=false;
    foreach (QString image, listImage) {
        QString imageName = image.mid(image.lastIndexOf('/')+1,image.length()-image.lastIndexOf('/')-5);
        qDebug()<<imageName;
        foreach (QString label, listLabel) {
            if(label.contains(imageName)){
                qDebug()<<label<<endl;
             QFile file(label);
             vector<cv::Rect> listRect;
             if(file.open(QIODevice::ReadOnly | QIODevice::Text))
             {

                 while(!file.atEnd()){
                     QString line =QString::fromLatin1(file.readLine());
                     staffCount++;
                     qDebug()<<line;

                     QStringList lines = line.split(" ");
                     qDebug()<<line.size();
                     cv::Rect rect;
                     rect.x =(int) lines.at(4).toFloat();
                     rect.y = (int)lines.at(5).toFloat();
                     rect.width = (int) lines.at(6).toFloat() - rect.x ;
                     rect.height = (int)lines.at(7).toFloat() -rect.y;
                     listRect.push_back(rect);
//                     cv::waitKey(0);
                 }
             }
             cv::Mat img = cv::imread(image.toStdString());

             for(int i=0; i<listRect.size();i++)
             {
                 cv::rectangle(img,listRect.at(i),cv::Scalar(0,255,0));
                 cv::putText(img,imageName.toStdString(),cv::Point(100,100),cv::FONT_HERSHEY_COMPLEX,2.0,cv::Scalar(0,255,0));
             }
             cv::imshow("img",img);
//             cout<<cv::waitKey(0)<<endl;
             if(cv::waitKey(waitkey) == 32)
             {
                 cv::waitKey(0);
             }
             else if(cv::waitKey(waitkey) ==255){
                 qDebug()<<"remove: "<<image<<" "<<label<<endl;
                 QFile::remove(image);
                 QFile::remove(label);
             }
            }

        }
//        qDebug()<<imageName<<pos<</*listLabel.at(pos)<<*/endl;
    }
        qDebug() << listImage.size()<<" "<<listLabel.size() <<"staff: "<<staffCount<<endl;
}
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
//    for(int i=0; i<argc;i++)
//    {
//        qDebug()<<argv[i]<<endl;
//    }
    if(argc == 4)
    {
        qDebug("test");
        int waitKey = atoi(argv[3]);
        if((QString)argv[1] == "test")
            test(argv[2],waitKey);
        return 0;
    }
    if(argc!=5)
    {
        qDebug()<<"input: [1] [2] [3] [4]"
                  "[1] - image folder"
                  "[2] - imglab xml file"
                  "[3] - des folder"
                  "[4] - ratio val/train = 1/[4] int value";
        a.quit();
    }else{
        QDomDocument doc("label");
        QFile xmlFile(argv[2]);
        if(!xmlFile.open(QIODevice::ReadOnly))
        {
            qDebug()<<"cant open "<<argv[2] <<"file";
            a.quit();
        }
        if(!doc.setContent(&xmlFile))
        {
            xmlFile.close();
            qDebug("cant handle xml");
            a.quit();
        }
        xmlFile.close();
        createTrainFolder(argv[3]);
        int count = 0;
        int ratio = ((QString)argv[4]).toInt();
        QString imageFoler = argv[1];
        QDomElement docElem = doc.documentElement();
//        QDomNodeList docNode = docElem.elementsByTagName("images")
        QDomNodeList images = docElem.elementsByTagName("images");
        QDomNodeList image = images.at(0).toElement().elementsByTagName("image");

        for(int i=0; i<image.size();i++)
        {
            count++;
            QString imagename = image.at(i).attributes().namedItem("file").nodeValue();
            int top,left,right,bot;
            QString kittilabel;
            QDomNodeList boxs = image.at(i).toElement().elementsByTagName("box");
            for(int j=0; j<boxs.size();j++)
            {
                QDomElement label = boxs.at(j).firstChildElement("label");
                top = boxs.at(j).attributes().namedItem("top").nodeValue().toInt();
                left = boxs.at(j).attributes().namedItem("left").nodeValue().toInt();
                right = boxs.at(j).attributes().namedItem("left").nodeValue().toInt() + boxs.at(j).attributes().namedItem("width").nodeValue().toInt();
                bot = boxs.at(j).attributes().namedItem("top").nodeValue().toInt() + boxs.at(j).attributes().namedItem("height").nodeValue().toInt();
                kittilabel = label.text();

                QString kittiLagbelPath;
                QString imageLabelName = imagename;
                if(count == ratio){
                    kittiLagbelPath = valLabel+imageLabelName.remove(".jpg")+".txt";
                }
                else{
                    kittiLagbelPath = trainLabel+imageLabelName.remove(".jpg")+".txt";
                }
                qDebug()<<"kittilabel: "<<kittiLagbelPath<<endl;
                QFile kittilabelfile(kittiLagbelPath);
                if(kittilabelfile.open(QIODevice::Append |QIODevice::Text))
                {
                    QTextStream stream(&kittilabelfile);
                    stream<<kittilabel<<" 0 0 0 "<<left<<" "<<top<<" "<<right<<" "<<bot<<" 0 0 0 0 0 0 0\n";
                    kittilabelfile.close();
                }else{
                    qDebug("cant open");
                }
            }
            qDebug()<<imagename;
            if(boxs.size()>0)
            {
                if(ratio == count)
                {
                    //copy to val
                    QFile::copy((imageFoler+imagename),(valImage + imagename));
    //                QFile::copy(kittiLagbelPath,valLabel+kittiLagbelPath);
                    count  = 0;
                }
                else {
                    QFile::copy((imageFoler+imagename),(trainImage + imagename));
    //                QFile::copy(kittiLagbelPath,trainLabel+kittiLagbelPath);
                }
            }

        }
        qDebug()<<image.size()<<" images"<< " "<<ratio;
    }

    return 0;
}
